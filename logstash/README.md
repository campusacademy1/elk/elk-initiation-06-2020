Ajouter ceci dans `config\pipeline.yml`

```
- pipeline.id: main
  path.config: "../pipelines/logstash-pipeline.conf"
```

Créer un dossier `pipelines` à la racine 

Ajouter un fichier `logstash-pipeline.conf` dans le dossier

Ajouter la definition des entrèes

```
input {
    file {
        path => [
            "C:/projects/coderbaseIT/formation-elk/product/logstash-7.7.1/pipelines/*.log",
            "C:/logstash/tp/data/*.log"
            ]
        start_position => "beginning"
    }
}

output {
    stdout {
        codec => json
    }
}
```
