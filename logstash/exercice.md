# Objectif
- Ajouter dans elastic des données qui provienent d'une api: https://kaamelott.chaudie.re/api/random
- Utilser le plugin d'input http_poller: https://www.elastic.co/guide/en/logstash/current/plugins-inputs-http_poller.html
- Output: 
    - stdout 👌
    - elasticsearch 1 index 👌
    - elasticsearch n index, 1 index par "saison" (ex: tp-elk-livre_i) 👌

- Ajouter un ou des filtres pour "lissée" les données, suprimmer les niveaux "citation" et "infos" 👌

En plus: 
- Trouver un moyen de changer le nom de l'index sans alterer la donnée ajouter dans elasticsearch (_piste_: metadata) 👌

En plus plus: 2 inputs, 2 outputs, filter different ⚠ Pensé a utilisé des conditions
- Ajouter un autre input (les fichiers qu'on avait avant) {log: string; level: string}
- S'assurer qu'on a bien un level
- Envoyer vers elastic dans différent index en fonction du level

En plus plus plus: 
- découper en 2 pipelines differents 